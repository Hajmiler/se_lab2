# Zakon dokument

## AUTOR: Ivan Hajmiler

[Wikipedia](https://www.wikipedia.org)

![slika šume](images/europeslostf.jpg)

### Što je audi?

Audi je njemački proizvođač automobila sa sjedištem u Ingolstadtu. Od 1964., Audi je marka unutar Volkswagen grupe.

Automobili proizvođača Audi se proizvode u Ingolstadtu, Neckarsulmu, Győru (Mađarska), Bratislavi (Slovačka), Sao José dos Pinhais (Brazil) i u Changchunu (Kina).



**August Horch**
 August Horch (1868. – 1951.), je 1899. osnovao marku Horch i počeo s proizvodnjom automobila, ali je u uskoro izbačen iz svoje kompanije pa je 1909. godine osnovao novu. Međutim, više nije imao pravo na korištenje svog obiteljskog imena pa je svoje prezime Horch preveo na latinski jezik. Riječ audi je imperativ od audire (na hrvatskom čuti), što i znači „Čuj!“ ili na njemačkom „Horch!“.

 ### Testni heading

 testni tekst