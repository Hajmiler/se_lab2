print ("Unosi imena ulica velicine izmedu 7 i 15 znakova dok se ne unese prekid")

ime_ulice=""
max=0
brojac_ulica=0

while ime_ulice!="prekid":

    ime_ulice=raw_input("Unesi ime ulice: ")
    while len(ime_ulice)<7 or len(ime_ulice)>15:
        ime_ulice=raw_input("Molim ponoviti upis: ")
        if ime_ulice=="prekid":
            break
    
    if len(ime_ulice)>max:
        max=len(ime_ulice)
        najveca_ulica=ime_ulice
    
    brojac_ulica+=1

print ("Uneseno je %d ulica" %(brojac_ulica-1))
print ("Ulica s najvise slova u nazivu je %s" %(najveca_ulica))
